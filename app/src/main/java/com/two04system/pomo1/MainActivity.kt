package com.two04system.pomo1

import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.ViewModelProvider
import com.two04system.pomo1.screen.ActivePomodoroScreen
import com.two04system.pomo1.service.PomoCountdownService
import com.two04system.pomo1.ui.theme.Pomo1Theme

class MainActivity : ComponentActivity() {
    private var pomCountdownService: PomoCountdownService? = null
    private var isBound = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Intent(this, PomoCountdownService::class.java).also { intent ->
            startService(intent)
        }

        // Observe the LiveData
        pomCountdownService?.pomoViewModel?.observe(this) { pomoViewModel ->
            // Update the UI
        }
        setContent {
            Pomo1Theme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    ActivePomodoroScreen()
                }
            }
        }
    }
}

@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = "Hello $name!",
        modifier = modifier
    )
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    Pomo1Theme {
        Greeting("Android")
    }
}