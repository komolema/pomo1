package com.two04system.pomo1.screen

import DEFAULT_POMO_CONFIG
import PomoConfig
import PomoTicker
import android.content.Intent
import android.os.Build
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.two04system.pomo1.PomoViewModel
import com.two04system.pomo1.service.PomoCountdownProvider
import com.two04system.pomo1.service.PomoCountdownService
import com.two04system.pomo1.ui.theme.Pomo1Theme
import androidx.compose.ui.platform.LocalContext
import org.mockito.Mockito

@Composable
fun ActivePomodoroScreen(pomoViewModel: PomoViewModel) {
    val context = LocalContext.current
    val pomoTicket = pomoViewModel.pomoTickerData.observeAsState()
    val countdownValue = remember {
        mutableStateListOf("25:00")
    }

    Column (modifier =  Modifier.fillMaxSize(), verticalArrangement = Arrangement.Center, horizontalAlignment = Alignment.CenterHorizontally){
        CountdownView(countdownValue.get(0))
        Button(onClick = {
            val intent = Intent(context, PomoCountdownService::class.java).apply {
                action = PomoCountdownService.ACTION_START_TICKING
            }
            context.startForegroundService(intent)

        }) {
            Text("Start")
        }
    }

}

@Preview(showBackground = true)
@Composable
fun ActivePomodoroScreenPreview() {
    val mockPomoViewModel = Mockito.mock(PomoViewModel::class.java)
    Pomo1Theme {
        ActivePomodoroScreen(mockPomoViewModel)
    }
}

@Composable
fun CountdownView(countdown: String) {
    Text(text = countdown)
}

