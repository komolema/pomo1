package com.two04system.pomo1

import androidx.lifecycle.MutableLiveData

class CountdownViewModel {
    val countdownValue = MutableLiveData("25:00")
}