package com.two04system.pomo1

import PomoTicker
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class PomoViewModel: ViewModel(){
    val pomoTickerData: MutableLiveData<PomoTicker> = MutableLiveData()
    val startTicking: MutableLiveData<Boolean> = MutableLiveData(false)
}