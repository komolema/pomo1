package com.two04system.pomo1.service

import PomoConfig

interface PomoCountdownProvider {
    fun startNewPomoCountdown()
}