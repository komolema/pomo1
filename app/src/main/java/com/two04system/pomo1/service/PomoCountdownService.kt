package com.two04system.pomo1.service

import DEFAULT_POMO_CONFIG
import PomoTicker
import PomoConfig
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.Build
import android.os.CountDownTimer
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.lifecycle.MutableLiveData
import com.two04system.pomo1.MainActivity
import com.two04system.pomo1.PomoTimer
import com.two04system.pomo1.PomoViewModel
import kotlinx.coroutines.launch

class PomoCountdownService : Service(), PomoCountdownProvider {

    private val TAG = "PomoCountdownService"

    val pomoViewModel = MutableLiveData<PomoViewModel>()

    private val pomoTimer = PomoTimer(25 * 60 * 1000, 1000, pomoViewModel)
    private val pomoConfig = MutableLiveData<PomoConfig>(DEFAULT_POMO_CONFIG)

    private lateinit var viewModel: PomoViewModel


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d(TAG, "onStartCommand called")

        when (intent?.action) {
            ACTION_START_TICKING -> {
                startNewPomoCountdown()
            }
        }

        return super.onStartCommand(intent, flags, startId)
    }

    override fun onCreate() {
        super.onCreate()
        Log.d(TAG, "PomoCountdownService created")

        // Create the notification channel
        createNotificationChannel()

        val intent = Intent(this, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }

        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_IMMUTABLE)

        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle("PomoCountdownService")
            .setContentText("PomoCountdownService is running")
            .setSmallIcon(android.R.drawable.ic_dialog_info)
            .setContentIntent(pendingIntent)
            .build()

        startForeground(1, notification)
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    private fun createNotificationChannel() {
        // Create a notification channel for the PomoCountdownService
        Log.d(TAG, "Creating notification channel")

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "PomoCountdownService Channel"
            val descriptionText = "Channel for PomoCountdownService"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                description = descriptionText
            }
            val notificationManager: NotificationManager =
                getSystemService(NotificationManager::class.java)
            notificationManager.createNotificationChannel(channel)
        }
    }

    companion object {
        const val CHANNEL_ID = "PomoCountdownServiceChannel"
        const val ACTION_START_TICKING = "ACTION_START_TICKING"
    }

    override fun startNewPomoCountdown() {
        // Start a new PomoTimer
        Log.d(TAG, "Starting new PomoTimer with default config: $pomoConfig")

        pomoTimer.start()
    }

    private fun pomoConfigToTicker(pomoConfig: PomoConfig): PomoTicker {
        return PomoTicker(pomoConfig.workDuration * 60)
    }
}

