package com.two04system.pomo1

import PomoTicker
import android.os.CountDownTimer
import android.util.Log
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch

class PomoTimer(
    timeInSeconds: Long,
    timeIntervalInSeconds: Long,
    private val pomoViewModel: MutableLiveData<PomoViewModel>,
)  {

    private val TAG = "PomoTimer"

    private val timer = object : CountDownTimer(timeInSeconds, timeIntervalInSeconds) {
        override fun onTick(millisUntilFinished: Long) {
            pomoViewModel.value?.pomoTickerData?.value = PomoTicker((millisUntilFinished / 1000).toInt())
            Log.d(TAG, "Tick: ${millisUntilFinished / 1000} seconds left")
        }

        override fun onFinish() {
            Log.d(TAG, "Timer finished")
        }
    }

    fun start() {
        timer.start()
        Log.d(TAG, "Timer started")
    }

}