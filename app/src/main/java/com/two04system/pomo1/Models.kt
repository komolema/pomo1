import java.time.LocalDateTime
import java.util.Optional
import java.util.UUID

data class Pomodoro(
    val id: UUID,
    val startTime: LocalDateTime,
    val endTime: Optional<LocalDateTime>,
    val isRunning: Boolean,
    val config: PomoConfig

)

data class PomodoroHistory(
    val pomodoros: List<Pomodoro>
)

data class Settings(
    val theme: PomoTheme,
    val pomodoroConfig: PomoConfig
)

enum class PomoTheme {
    LIGHT, DARK
}

data class PomoConfig(
    val workDuration: Int,
    val shortBreakDuration: Int,
    val longBreakDuration: Int,
    val longBreakInterval: Int
)

data class PomoTicker(
    val secondsLeft: Int,
)

const val DEFAULT_WORK_DURATION = 25
const val DEFAULT_SHORT_BREAK_DURATION = 5
const val DEFAULT_LONG_BREAK_DURATION = 15
const val DEFAULT_LONG_BREAK_INTERVAL = 4

val DEFAULT_POMO_CONFIG = PomoConfig(DEFAULT_WORK_DURATION, DEFAULT_SHORT_BREAK_DURATION, DEFAULT_LONG_BREAK_DURATION, DEFAULT_LONG_BREAK_INTERVAL)
