package com.two04system.pomo1

import androidx.test.ext.junit.runners.AndroidJUnit4
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify

@RunWith(AndroidJUnit4::class)
class PomoTimerTest {

    private val testDispatcher = TestCoroutineDispatcher()
    private val testScope = CoroutineScope(testDispatcher)

    @Test
    fun OnTickSendToChannel() = runBlocking(Dispatchers.Main) {
        val mockChannel: Channel<Long> = mock()
        val pomoTimer = PomoTimer(1500000, 1000, mockChannel, testScope)

        pomoTimer.start()

        verify(mockChannel).send(1000)
    }
}